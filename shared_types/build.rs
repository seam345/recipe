use crux_core::typegen::TypeGen;
use shared::ui_model::{UiScreen, UiIngredientUnit};
use shared::{IngredientUnit, RecipePlanetApp, StandardIngredient};
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=../shared");

    let mut gen = TypeGen::new();

    gen.register_app::<RecipePlanetApp>().expect("register");
    gen.register_type::<UiScreen>().expect("register");
    gen.register_type::<IngredientUnit>().expect("register");
    gen.register_type::<UiIngredientUnit>().expect("register");
    // gen.register_type::<Result<Vec<StandardIngredient>, String>>().expect("register");

    let output_root = PathBuf::from("./generated");

    // gen.swift("SharedTypes", output_root.join("swift"))
    //     .expect("swift type gen failed");

    gen.java(
        "com.redbadger.catfacts.shared_types",
        output_root.join("java"),
    )
    .expect("java type gen failed");

    gen.typescript("shared_types", output_root.join("typescript"))
        .expect("typescript type gen failed");
}
