mod core;
mod http;
mod platform;
mod time;
mod screens {
    pub mod generated_page;
    pub mod landing_page;
}

use crate::core::{Core, Message};
use crate::screens::{generated_page, landing_page};
use gloo_console::{info, trace};
use shared::ui_model::UiScreen;
use shared::{Event, RecipePlanetApp};
use wasm_bindgen::JsValue;
use yew::prelude::*;

#[derive(Default)]
pub struct RootComponent {
    core: Core,
}

impl Component for RootComponent {
    type Message = Message;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link();
        link.send_message(Message::Event(Event::GetLandingPage));
        link.send_message(Message::Event(Event::GetPlatform));

        Self { core: core::new() }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let link = ctx.link().clone();
        let callback = Callback::from(move |msg| {
            link.send_message(msg);
        });
        if let Message::Event(event) = msg {
            core::update(&self.core, event, &callback);
            false
        } else {
            true
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let view = self.core.view();

        println!("{}", &view.platform);

        match &view.screen {
            UiScreen::LandingPage => landing_page::render_page(&view, link),
            UiScreen::GenerateRecipe(recipe) => generated_page::render_page(link, recipe),
        }
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());

    let object = JsValue::from("world");
    info!("Hello {}", object.as_string().unwrap());
    yew::Renderer::<RootComponent>::new().render();
}
