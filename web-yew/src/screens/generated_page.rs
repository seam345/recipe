use crate::core::{Core, Message};
use crate::RootComponent;
use shared::ui_model::UiRecipe;
use shared::{Event, RecipePlanetApp};
use yew::html::Scope;
use yew::virtual_dom::VNode;
use yew::Html;
use yew::{html, BaseComponent};

pub fn render_page(link: &Scope<RootComponent>, recipe: &UiRecipe) -> VNode {
    html! {
        <>
            <section class="section title has-text-centered">
                <p>{&recipe.name}</p>
            </section>

           <section class="section container">
                <div class="columns">
                    <div class="column is-one-fifth content has-background-light">
                        <h2>{"Ingredients"}</h2>
                        <ul>
                            {
                                recipe.ingredients.iter().map(|ingredient| {
                                    html!{
                                        <li>
                                            {format!("{}: {} {}", ingredient.name, ingredient.quantity, ingredient.unit)}
                                        </li>
                                    }
                                }).collect::<Html>()
                            }
                        </ul>
                    </div>
                    <div class="column">
                        <h2 class="has-text-centered title">{"Instructions"}</h2>
                        {
                            recipe.instructions.iter().map(|instruction| {
                                html!{
                                    <section class="section container">
                                        {instruction}
                                    </section>
                                }
                            }).collect::<Html>()
                        }
                    </div>
                </div>
            </section>
            <button class="button is-primary is-warning"
                onclick={link.callback(|_| Message::Event(Event::GetLandingPage))}>
                {"Go back"}
            </button>
        </>
    }
}
