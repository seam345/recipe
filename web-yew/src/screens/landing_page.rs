use crate::core::{Core, Message};
use crate::RootComponent;
use shared::{Event, Ingredient, RecipePlanetApp, StandardIngredient};
use yew::html::Scope;
use yew::virtual_dom::VNode;
use yew::{html, BaseComponent};

//todo asses if i need view, maybe i only need the screen data
pub fn render_page(
    view: &<RecipePlanetApp as shared::App>::ViewModel,
    link: &Scope<RootComponent>,
) -> VNode {
    html! {
        <>
            <section class="section title has-text-centered">
                <p>{"Welcome to Recipe Planet!"}</p>
            </section>
            <section class="section container has-text-centered">
                <p>{"What shall we do today?"}</p>
            </section>
            <section class="section container has-text-centered">
                <div class="columns">
                    <div class="column">
                        <button class="button is-info"
                            onclick={link.callback(|_| Message::Event(Event::GetAddRecipePage))}>
                            {"Add Recipe"}
                        </button>
                    </div>
                    <div class="column">
                        <button class="button is-primary"
                            onclick={link.callback(|_| Message::Event(Event::GetGenerateRecipePage(get_ingredients())))}>
                            {"Generate new recipe"}
                        </button>
                    </div>
                    <div class="column">
                        <button class="button is-info"
                            onclick={link.callback(|_| Message::Event(Event::GetUpdateSIPage))}>
                            {"Add Standard ingredient"}
                        </button>
                    </div>
                </div>
            </section>
        </>
    }
}

fn get_ingredients() -> Vec<StandardIngredient> {
    vec![
        StandardIngredient {
            name: "Banana".to_string(),
            nicknames: vec!["Bananas".to_owned()],
            standard_unit: Default::default(),
            su_grams: Some(50),
            su_ml: None,
        },
        StandardIngredient {
            name: "".to_string(),
            nicknames: vec![],
            standard_unit: Default::default(),
            su_grams: None,
            su_ml: None,
        },
    ]
}
