use gloo_console::{info, log};
use shared::capabilities::recipe::RecipeOperationOutput;
use shared::capabilities::standard_ingredient::SIOperationOutput;
use shared::{
    key_value::{KeyValueOperation, KeyValueOutput},
    platform::PlatformResponse,
    time::TimeResponse,
    Effect, Event, Recipe, RecipePlanetApp, RecipePlanetCapabilities, StandardIngredient,
};
use std::rc::Rc;
use web_sys::console::info;
use yew::{platform::spawn_local, Callback};

use crate::{http, platform, time};

pub type Core = Rc<shared::Core<Effect, RecipePlanetApp>>;

pub enum Message {
    Event(Event),
    Effect(Effect),
}

pub fn new() -> Core {
    Rc::new(shared::Core::new::<RecipePlanetCapabilities>())
}

pub fn update(core: &Core, event: Event, callback: &Callback<Message>) {
    log!(format!("event: {:?}", event));
    for effect in core.process_event(event) {
        process_effect(core, effect, callback);
    }
}

pub fn process_effect(core: &Core, effect: Effect, callback: &Callback<Message>) {
    log!(format!("effect: {:?}", effect));
    match effect {
        render @ Effect::Render(_) => callback.emit(Message::Effect(render)),
        Effect::Http(mut request) => {
            spawn_local({
                let core = core.clone();
                let callback = callback.clone();

                async move {
                    let response = http::request(&request.operation).await.unwrap();

                    for effect in core.resolve(&mut request, response) {
                        process_effect(&core, effect, &callback);
                    }
                }
            });
        }

        Effect::KeyValue(mut request) => {
            let response = match request.operation {
                KeyValueOperation::Read(_) => KeyValueOutput::Read(None),
                KeyValueOperation::Write(_, _) => KeyValueOutput::Write(false),
            };

            for effect in core.resolve(&mut request, response) {
                process_effect(&core, effect, &callback);
            }
        }

        Effect::Platform(mut request) => {
            let response =
                PlatformResponse(platform::get().unwrap_or_else(|_| "Unknown browser".to_string()));

            for effect in core.resolve(&mut request, response) {
                process_effect(&core, effect, &callback);
            }
        }

        Effect::Time(mut request) => {
            let response = TimeResponse(time::get().unwrap());

            for effect in core.resolve(&mut request, response) {
                process_effect(&core, effect, &callback);
            }
        }

        Effect::CapStandardIngredient(mut request) => {
            let response = SIOperationOutput::GotIngredientList(vec![
                StandardIngredient {
                    name: "Banana".to_string(),
                    nicknames: vec!["Bananas".to_owned()],
                    standard_unit: Default::default(),
                    su_grams: Some(50),
                    su_ml: None,
                },
                StandardIngredient {
                    name: "".to_string(),
                    nicknames: vec![],
                    standard_unit: Default::default(),
                    su_grams: None,
                    su_ml: None,
                },
            ]);

            for effect in core.resolve(&mut request, response) {
                process_effect(&core, effect, &callback);
            }
        }

        Effect::CapRecipe(mut request) => {
            info!("cap Recipe called from shell");
            let response = RecipeOperationOutput::ReturnedRecipes(vec![
                Recipe {
                    name: "Recipe 1".to_owned(),
                    instructions: "Some instructions for recipe 1".to_owned(),
                    ingredients: vec![],
                },
                Recipe {
                    name: "Recipe 2".to_owned(),
                    instructions: "Some instructions for recipe 2".to_owned(),
                    ingredients: vec![],
                },
            ]);

            for effect in core.resolve(&mut request, response) {
                process_effect(&core, effect, &callback);
            }
        }
    }
}
