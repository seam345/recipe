use std::fmt::{Display, Formatter};
use serde::{Deserialize, Serialize};
use crate::{IngredientUnit, Recipe};

/// Model used by UI, in a separate file because the plan is it will mainly be owned/defined by Sean
/// with Poppy constructing it from her internal Model

#[derive(Serialize, Deserialize, Default)]
pub struct UiModel {
    pub screen: UiScreen,
    pub platform: String,
}

#[derive(Serialize, Deserialize)]
pub enum UiScreen {
    LandingPage,
    GenerateRecipe(UiRecipe),
}

impl Default for UiScreen {
    fn default() -> Self {
        UiScreen::LandingPage
    }
}

#[derive(Serialize, Deserialize)]
pub struct UiRecipe {
    pub name: String,
    pub instructions: Vec<String>,
    pub ingredients: Vec<UiIngredient>, // todo Think sean made a mistake here and it should be Ingredient instead as that has a quantity attached
}

#[derive(Serialize, Deserialize)]
pub struct UiIngredient {
    // todo do i want to store an ID to standard ingredient? this feels unnecessary if its in model
    pub unit: UiIngredientUnit,
    pub quantity: f32,
    pub name: String,
}

#[derive(Serialize, Deserialize, Default)]
pub enum UiIngredientUnit {
    #[default]
    Item,
    Small,
    Medium,
    Large,
    Gram,
    Kilogram,
    Teaspoon,
    Tablespoon,
    Millilitre,
}

impl From<IngredientUnit> for UiIngredientUnit {
    fn from(unit: IngredientUnit) -> Self {
        match unit {
            IngredientUnit::Item => UiIngredientUnit::Item,
            IngredientUnit::Small => UiIngredientUnit::Small,
            IngredientUnit::Medium => UiIngredientUnit::Medium,
            IngredientUnit::Large => UiIngredientUnit::Large,
            IngredientUnit::Gram => UiIngredientUnit::Gram,
            IngredientUnit::Kilogram => UiIngredientUnit::Kilogram,
            IngredientUnit::Teaspoon => UiIngredientUnit::Teaspoon,
            IngredientUnit::TableSpoon => UiIngredientUnit::Tablespoon,
            IngredientUnit::Millilitre => UiIngredientUnit::Millilitre,
        }
    }
}

impl Display for UiIngredientUnit {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            UiIngredientUnit::Item => f.write_str(""),
            UiIngredientUnit::Small => f.write_str("Small"),
            UiIngredientUnit::Medium => f.write_str("Medium"),
            UiIngredientUnit::Large => f.write_str("Large"),
            UiIngredientUnit::Gram => f.write_str("g"),
            UiIngredientUnit::Kilogram => f.write_str("kg"),
            UiIngredientUnit::Teaspoon => f.write_str("tsp"),
            UiIngredientUnit::Tablespoon => f.write_str("tbsp"),
            UiIngredientUnit::Millilitre => f.write_str("ml"),
        }
    }
}
