use crux_core::capability::Operation;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum RecipeOperation {
    GetRecipesThatContainsOneOfIngredientList(Vec<StandardIngredient>), // list of ingredient names
    GetRecipesThatContainsOneOfIngredient(StandardIngredient),          //  ingredient names
    GetRecipe(String), // get recipe by name (maybe need a better unique identifier
                       // SetIngredient,
}

// cannot pass result through FFI so i have an error type and I shall construct a result based on what is returned
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum RecipeOperationOutput {
    ReturnedRecipes(Vec<Recipe>),
    ReturnedRecipe(Recipe),
    // SetResult(bool),
    Error(String)
}

impl Operation for RecipeOperation {
    type Output = RecipeOperationOutput;
}

use crate::{Ingredient, Recipe, StandardIngredient};
use crux_core::capability::CapabilityContext;
use crux_macros::Capability;
use uniffi::deps::log::{info, trace};

#[derive(Capability)]
pub struct CapRecipe<Ev> {
    context: CapabilityContext<RecipeOperation, Ev>,
}

impl<Ev> CapRecipe<Ev>
where
    Ev: 'static,
{
    pub fn new(context: CapabilityContext<RecipeOperation, Ev>) -> Self {
        Self { context }
    }

    pub fn get_recipes_from_ingredients_list<F>(&self, ingredients: Vec<StandardIngredient>, event: F)
    where
        F: Fn(Result<Vec<Recipe>, String>) -> Ev + std::marker::Send + 'static,
    {
        info!("get_recipes_from_ingredients_list called");
        let ctx = self.context.clone();
        self.context.spawn(async move {
            info!("get_recipes_from_ingredients_list spawned");
            let response = ctx
                .request_from_shell(RecipeOperation::GetRecipesThatContainsOneOfIngredientList(ingredients))
                .await;
            // let RecipeOperationOutput::GetData(return_data) = response else {
            //     panic!("Seany messed up")
            // };

            let return_data = Ok(vec![
                Recipe {
                    name: "Recipe 1".to_owned(),
                    instructions: "Some instructions for recipe 1".to_owned(),
                    ingredients: vec![],
                },
                Recipe {
                    name: "Recipe 2".to_owned(),
                    instructions: "Some instructions for recipe 2".to_owned(),
                    ingredients: vec![],
                },
            ]);

            ctx.update_app(event(return_data));
        });
    }
}
