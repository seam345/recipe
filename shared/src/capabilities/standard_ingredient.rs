use crate::StandardIngredient;
use crux_core::capability::CapabilityContext;
use crux_core::capability::Operation;
use crux_macros::Capability;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum SIOperation {
    GetIngredientList(Vec<String>),
    GetIngredient(String),
    // SetIngredient,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum SIOperationOutput {
    GotIngredientList(Vec<StandardIngredient>),
    // SetResult(bool),
    Error(String)
}

impl Operation for SIOperation {
    type Output = SIOperationOutput;
}

#[derive(Capability)]
pub struct CapStandardIngredient<Ev> {
    context: CapabilityContext<SIOperation, Ev>,
}

impl<Ev> CapStandardIngredient<Ev>
where
    Ev: 'static,
{
    pub fn new(context: CapabilityContext<SIOperation, Ev>) -> Self {
        Self { context }
    }

    pub fn get_ingredient_list<F>(&self, ingredients: String, event: F)
    where
        F: Fn(Result<Vec<StandardIngredient>, String>) -> Ev + std::marker::Send + 'static,
    {
        let ctx = self.context.clone();
        self.context.spawn(async move {
            // let response = ctx
            //     .request_from_shell(SIOperation::GetIngredientList(ingredients))
            //     .await;
            // let SIOperationOutput::GetData(return_data) = response else {
            //     panic!("Seany messed up")
            // };

            let return_data = Ok(vec![
                StandardIngredient {
                    name: "Banana".to_string(),
                    nicknames: vec!["Bananas".to_owned()],
                    standard_unit: Default::default(),
                    su_grams: Some(50),
                    su_ml: None,
                },
                StandardIngredient {
                    name: "".to_string(),
                    nicknames: vec![],
                    standard_unit: Default::default(),
                    su_grams: None,
                    su_ml: None,
                },
            ]);
            ctx.update_app(event(return_data));
        });
    }
}
