use uniffi::deps::log::{info, trace};
use crate::{StandardIngredient, Model, RecipePlanetApp, RecipePlanetCapabilities, Event};

pub fn trigger(
    ingredients: Vec<StandardIngredient>,
    app: &RecipePlanetApp,
    model: &mut Model,
    caps: &RecipePlanetCapabilities,
) {
    info!("Get_generate_recipe called");
    caps.recipe.get_recipes_from_ingredients_list(ingredients, Event::GotRecipes)


}

#[cfg(test)]
mod tests {
    use assert_let_bind::assert_let;
    use crux_core::testing::AppTester;
    use crux_http::{
        protocol::{HttpRequest, HttpResponse},
        testing::ResponseBuilder,
    };

    use crate::{Effect, Event, RecipePlanetApp};

    use super::*;

    #[test]
    fn fetch_sends_some_requests() {
        let app = AppTester::<RecipePlanetApp, _>::default();
        let mut model = Model::default();

        // due to capabilities being private i have to call it idirectly... booo
        let update = app.update(Event::GetGenerateRecipePage(vec![]), &mut model);

        // trigger(vec!(), &app, &mut model,&app.capabilities )

        assert_let!(Effect::Http(request), update.effects().next().unwrap());
        let actual = &request.operation;
        // let expected = &HttpRequest::get(FACT_API_URL).build();

        // assert_eq!(actual, expected);
    }
}
