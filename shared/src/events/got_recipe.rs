use uniffi::deps::log::info;
use crate::{Model, RecipePlanetApp, RecipePlanetCapabilities, Event, Recipe, Screen};

pub fn trigger(
    recipe_result: Result<Vec<Recipe>, String>,
    app: &RecipePlanetApp,
    model: &mut Model,
    caps: &RecipePlanetCapabilities,
) {
    info!("Got recipe called");
    // todo add the recipe data to the model
    model.screen = Screen::GenerateRecipe;

    caps.render.render();

}