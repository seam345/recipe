use crate::{Model, RecipePlanetApp, RecipePlanetCapabilities, Screen};

pub fn trigger(app: &RecipePlanetApp, model: &mut Model, caps: &RecipePlanetCapabilities) {
    model.screen = Screen::LandingPage;
    caps.render.render();
}
