use crate::{
    Model, PossibleIngredientName, PossibleUnit, QuantityUnit, RecipePlanetApp,
    RecipePlanetCapabilities,
};

pub fn trigger(
    recipe_name: String,
    possible_ingredients: Vec<(PossibleIngredientName, PossibleUnit, QuantityUnit)>,
    app: &RecipePlanetApp,
    model: &mut Model,
    caps: &RecipePlanetCapabilities,
) {
}
