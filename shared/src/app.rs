pub mod platform;

use serde::{Deserialize, Serialize};

pub use crux_core::App;
use crux_core::{render::Render, Capability};
use crux_http::Http;
use crux_kv::{KeyValue, KeyValueOutput};
use crux_macros::Effect;
use crux_platform::Platform;
use crux_time::{Time, TimeResponse};

use crate::capabilities::standard_ingredient;
use crate::events::{add_recipe, get_add_recipe_page, get_generate_recipe_page, get_landing_page, get_update_standard_ingredient_page, got_recipe, populate_ingredients};
use crate::ui_model::{UiScreen, UiModel, UiRecipe, UiIngredient, UiIngredientUnit};
use platform::Capabilities;
use crate::capabilities::recipe::CapRecipe;
use crate::capabilities::standard_ingredient::CapStandardIngredient;

#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct StandardIngredient {
    pub name: String,
    pub nicknames: Vec<String>,
    pub standard_unit: IngredientUnit,
    pub su_grams: Option<u32>,
    pub su_ml: Option<u32>,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct Recipe {
    pub name: String,
    pub instructions: String,
    pub ingredients: Vec<StandardIngredient>, // todo Think sean made a mistake here and it should be Ingredient instead as that has a quantity attached
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub struct Ingredient {
    pub standard_ingredient: StandardIngredient,
    pub unit: IngredientUnit,
    pub quantity: f32,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, PartialEq)]
pub enum IngredientUnit {
    #[default]
    Item,
    Small,
    Medium,
    Large,
    Gram,
    Kilogram,
    Teaspoon,
    TableSpoon,
    Millilitre,
}

/// Global UI data
/// pub(crate) as it should never get sent to the shell, that is what the ViewModel is for.
///
/// for some reason we need model to be public but we can still keep the internals private :/ something to understand why
#[derive(Serialize, Deserialize, Default)]
pub struct Model {
    pub(crate) screen: Screen,
    pub(crate) platform: platform::Model,
}

#[derive(Serialize, Deserialize)]
pub(crate) enum Screen {
    LandingPage,
    GenerateRecipe,
}

impl Default for Screen {
    fn default() -> Self {
        Screen::LandingPage
    }
}

pub type PossibleIngredientName = String;
pub type PossibleUnit = String;
pub type QuantityUnit = String;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub enum Event {
    // Load Pages
    GetLandingPage,
    GetAddRecipePage,
    GetGenerateRecipePage(Vec<StandardIngredient>),
    GetUpdateSIPage,

    // AddRecipe
    AddRecipe(
        String,
        Vec<(PossibleIngredientName, PossibleUnit, QuantityUnit)>,
    ), // Send back a Recipe
    // ComfirmRecipe(Recipe), // Shows it, asks you to save it
    #[serde(skip)]
    PopulateIngredients(String), //todo sean to change type Sting to something more usable

    // GenerateRecipe
    #[serde(skip)]
    GotRecipes(Result<Vec<Recipe>, String>),

    // UpdateSI
    AddStandardIngredient(StandardIngredient),

    None,
    GetPlatform,
    Restore, // restore state

    // events local to the core
    Platform(platform::Event),
    SetState(KeyValueOutput), // receive the data to restore state with
    CurrentTime(TimeResponse),
}

#[derive(Default)]
pub struct RecipePlanetApp {
    platform: platform::App,
}

#[cfg_attr(feature = "typegen", derive(crux_macros::Export))]
#[derive(Effect)]
#[effect(app = "RecipePlanetApp")]
pub struct RecipePlanetCapabilities {
    pub platform: Platform<Event>,
    pub http: Http<Event>,
    pub key_value: KeyValue<Event>,
    pub render: Render<Event>,
    pub time: Time<Event>,
    pub ing: CapStandardIngredient<Event>,
    pub recipe: CapRecipe<Event>,
}

// Allow easily using Platform as a submodule
impl From<&RecipePlanetCapabilities> for Capabilities {
    fn from(incoming: &RecipePlanetCapabilities) -> Self {
        Capabilities {
            platform: incoming.platform.map_event(super::Event::Platform),
            render: incoming.render.map_event(super::Event::Platform),
        }
    }
}

impl App for RecipePlanetApp {
    type Event = Event;
    type Model = Model;
    type ViewModel = UiModel;
    type Capabilities = RecipePlanetCapabilities;

    fn update(&self, msg: Event, model: &mut Model, caps: &RecipePlanetCapabilities) {
        match msg {
            // Event::ComfirmRecipe(_) => todo!(),
            Event::GetPlatform => {
                self.platform
                    .update(platform::Event::Get, &mut model.platform, &caps.into())
            }
            Event::Platform(msg) => self.platform.update(msg, &mut model.platform, &caps.into()),
            Event::CurrentTime(_iso_time) => {      },
            Event::Restore => {
                caps.key_value.read("state", Event::SetState);
            }
            Event::SetState(response) => {
                if let KeyValueOutput::Read(Some(bytes)) = response {
                    if let Ok(m) = serde_json::from_slice::<Model>(&bytes) {
                        *model = m
                    };
                }

                caps.render.render()
            }
            Event::None => {}

            Event::GetLandingPage => get_landing_page::trigger(self, model, caps),
            Event::GetAddRecipePage => get_add_recipe_page::trigger(self, model, caps),
            Event::GetGenerateRecipePage(ingredients) =>
                get_generate_recipe_page::trigger(ingredients, self, model, caps),
            Event::GetUpdateSIPage =>
                get_update_standard_ingredient_page::trigger(self, model, caps),
            Event::AddRecipe(recipe_name, possible_ingredients) =>
                add_recipe::trigger(recipe_name, possible_ingredients, self, model, caps),
            Event::PopulateIngredients(ingredient_json) =>
                populate_ingredients::trigger(ingredient_json, self, model, caps),
            Event::AddStandardIngredient(_) => todo!(),
            Event::GotRecipes(recipe_result) => got_recipe::trigger(recipe_result, self, model, caps),
        }
    }

    fn view(&self, model: &Model) -> UiModel {
        let platform =
            <platform::App as crux_core::App>::view(&self.platform, &model.platform).platform;
        match &model.screen {
            Screen::LandingPage => UiModel {
                platform,
                screen: UiScreen::LandingPage,
            },
            Screen::GenerateRecipe => UiModel {
                platform,
                screen: UiScreen::GenerateRecipe(
                    // the below is just some dummy data for rendering the UI
                    UiRecipe{
                        name: "Dummy recipe".to_string(),
                        instructions: vec!["Some long string of instructions to follow... ".to_owned(), "another set of instructions to follow".to_owned()],
                        ingredients: vec![
                            UiIngredient{
                                name: "ingredient 1".to_owned(),
                                unit: Default::default(),
                                quantity: 1.0,
                            },
                            UiIngredient{
                                name: "ingredient 2".to_owned(),
                                unit: UiIngredientUnit::Teaspoon,
                                quantity: 1.5,
                            }
                        ],
                    }
                ),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use assert_let_bind::assert_let;
    use crux_core::testing::AppTester;
    use crux_http::{
        protocol::{HttpRequest, HttpResponse},
        testing::ResponseBuilder,
    };

    use crate::Effect;

    use super::*;

    #[test]
    fn fetch_sends_some_requests() {
        /* let app = AppTester::<RecipePlanetApp, _>::default();
        let mut model = Model::default();

        let update = app.update(Event::Fetch, &mut model);

        assert_let!(Effect::Http(request), update.effects().next().unwrap());
        let actual = &request.operation;
        let expected = &HttpRequest::get(FACT_API_URL).build();

        assert_eq!(actual, expected);*/
    }

    #[test]
    fn fetch_results_in_set_fact_and_set_image() {
        /*let app = AppTester::<RecipePlanetApp, _>::default();
        let mut model = Model::default();

        let mut update = app.update(Event::Fetch, &mut model);
        let mut effects = update.effects_mut();

        assert_let!(Effect::Http(request), effects.next().unwrap());
        let actual = &request.operation;
        let expected = &HttpRequest::get(FACT_API_URL).build();
        assert_eq!(actual, expected);

        let a_fact = CatFact {
            fact: "cats are good".to_string(),
            length: 13,
        };

        let response = HttpResponse::ok()
            .body(r#"{ "fact": "cats are good", "length": 13 }"#)
            .build();
        let update = app
            .resolve(request, response)
            .expect("should resolve successfully");

        let expected_response = ResponseBuilder::ok().body(a_fact.clone()).build();
        assert_eq!(update.events, vec![Event::SetFact(Ok(expected_response))]);

        for event in update.events {
            app.update(event, &mut model);
        }

        assert_let!(Effect::Http(request), effects.next().unwrap());
        let actual = &request.operation;
        let expected = &HttpRequest::get(IMAGE_API_URL).build();
        assert_eq!(actual, expected);

        let a_image = CatImage {
            href: "image_url".to_string(),
        };

        let response = HttpResponse::ok().body(r#"{"href":"image_url"}"#).build();
        let update = app
            .resolve(request, response)
            .expect("should resolve successfully");
        for event in update.events {
            app.update(event, &mut model);
        }

        assert_eq!(model.cat_fact, Some(a_fact));
        assert_eq!(model.cat_image, Some(a_image));*/
    }
}
