pub mod app;
pub mod ui_model;

pub mod capabilities {
    pub mod recipe;
    pub mod standard_ingredient;
}
pub mod events {
    pub mod add_recipe;
    pub mod get_add_recipe_page;
    pub mod get_generate_recipe_page;
    pub mod get_landing_page;
    pub mod get_update_standard_ingredient_page;
    pub mod populate_ingredients;
    pub mod got_recipe;
}

use lazy_static::lazy_static;
use wasm_bindgen::prelude::wasm_bindgen;

use crux_core::bridge::Bridge;
pub use crux_core::{Core, Request};
pub use crux_http as http;
pub use crux_kv as key_value;
pub use crux_platform as platform;
pub use crux_time as time;

pub use app::*;

// TODO hide this plumbing

uniffi::include_scaffolding!("shared");

lazy_static! {
    static ref CORE: Bridge<Effect, RecipePlanetApp> =
        Bridge::new(Core::new::<RecipePlanetCapabilities>());
}

#[wasm_bindgen]
pub fn process_event(data: &[u8]) -> Vec<u8> {
    CORE.process_event(data)
}

#[wasm_bindgen]
pub fn handle_response(uuid: &[u8], data: &[u8]) -> Vec<u8> {
    CORE.handle_response(uuid, data)
}

#[wasm_bindgen]
pub fn view() -> Vec<u8> {
    CORE.view()
}
